<?php
/**
 * @href URL of the thread
 * @parent_href URL to the parent of the topic
 * @parent Parent of the topic
 * @topic Topic
 * @posted Date of post
 * @author Author of the topic
 * @body The body of the topic
 */
?>



<span class="lithium_topictype"><?php print t('Discussion:')?>
	<a href="<?php print $parent_href?>"><?php print $parent?></a>
</span>

<p class="lithium_topic"><a href="<?php print $href?>"><?php print $topic?></a></p>

<span class="lithium_posts"><?php print $posted?></span> 

<div class="lithium_comment">

	<img src="<?php print base_path() . path_to_theme()?>/images/discussion_bubble_top2.png">
	<div class="lithium_comment_bub">
	
		<span><?php print $author?></span>
	
		<p>
			<a href="<?php print $href?>">
				<span><?php print $body?></span>
			</a>
		</p>

	</div>
	
</div>
