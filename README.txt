01/11/2010 rvarkonyi

Integration between Lithium boards and Drupal

The module creates an authentication cookie in Drupal and sets it in the client browser for Lithium to decrypt.

It supports syncing Drupal profile fields with the following Lithium fields:

 - Location
 - First name
 - Last name
 - Birthday
 - Avatar (from user's profile, make sure you enable user pictures in Drupal)
 - Password remember - auto-login feature in Lithium

Lithium files
------------

Lithium should send you a file called lithium_sso.php that contains the lithium_sso class which you'll need
in order to ecnrypt the user information you'll send to them. This file needs to be in a folder called Lithium_SSO
that you'll need to create under your module folder so eventually it looks like this:

sites/all/modules/lithium/Lithium_SSO


Configuration
------------

After installing make sure to visit the module's configuration page (Site configuration - Lithium board settings) 
where you need to fill out the required information regarding your Lithium settings. 
 
Permissions
------------

Make sure you add 'Access Lithium board settings' permission to the relevant user roles


Blocks
------------

The module creates 2 blocks:

	- Featured topic:
	
	  Using REST API the block displays the featured topics 
	  
	- Top boards:
	
	  Using REST API the block displays the most commented on topics
	  
	    