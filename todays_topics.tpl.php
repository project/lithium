<?php 
	/**
	 * @href URL of the thread
	 * @replies Replies
	 * @topic Topic
	 */
?>
<div class="field-item">
	<a href="<?php print $href?>">
		<div class="replies">
			<?php print $replies?>
		</div>
		<div class="topic">
			<?php print $topic?>
		</div>
	</a>
</div>
<div class="clear"></div>